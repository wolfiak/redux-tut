import { actionCreators } from './todo';
const MESSAGE_SHOW = 'MESSAGE_SHOW';

export const showMessage = msg => ({ type: MESSAGE_SHOW, payload: msg });

export default (state = '', action) => {
  switch (action.type) {
    case MESSAGE_SHOW:
      return action.payload;
    case actionCreators.todoAdd:
      return '';
    case actionCreators.todoLoad:
      return '';
    case actionCreators.todoReplace:
    case actionCreators.todoDelete:
      return '';
    default:
      return state;
  }
};
