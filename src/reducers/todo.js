import {
  getTodos,
  createTodo,
  updateTodo,
  deleteTodo
} from "../lib/todoServices";
import { showMessage } from "./messages";
const initState = {
  todos: [],
  currentTodo: ""
};

export const actionCreators = {
  currentUpdate: "CURRENT_UPDATE",
  todoAdd: "TODO_ADD",
  todoLoad: "TODO_LOAD",
  todoReplace: "TODO_REPLACE",
  todoDelete: "TODO_DELETE"
};

export const updateCurrent = val => ({
  type: actionCreators.currentUpdate,
  payload: val
});
export const loadTodos = todos => ({
  type: actionCreators.todoLoad,
  payload: todos
});
export const addTodo = todo => ({
  type: actionCreators.todoAdd,
  payload: todo
});
export const replaceTodo = todo => ({
  type: actionCreators.todoReplace,
  payload: todo
});
export const deleteTodoAction = id => ({
  type: actionCreators.todoDelete,
  payload: id
});

export const postTodo = name => {
  return dispatch => {
    dispatch(showMessage("Saving todo"));
    createTodo(name).then(todo => dispatch(addTodo(todo)));
  };
};
export const toggleTodo = id => {
  return (dispatch, getState) => {
    dispatch(showMessage("Update todo"));
    const { todos } = getState().todo;
    const todo = todos.find(t => t.id === id);
    const toggled = { ...todo, isComplete: !todo.isComplete };
    console.log("toggled", toggled);
    updateTodo(toggled).then(todo => dispatch(replaceTodo(todo)));
  };
};

export const removeTodo = id => {
  return dispatch => {
    dispatch(showMessage("Delete todo"));
    deleteTodo(id).then(() => dispatch(deleteTodoAction(id)));
  };
};

export const fetchTodos = () => {
  return dispatch => {
    dispatch(showMessage("Loading todo"));
    getTodos().then(todos => dispatch(loadTodos(todos)));
  };
};

export const getVisibleTodos = (todos, filter) => {
  switch (filter) {
    case "active":
      return todos.filter(t => !t.isComplete);
    case "completed":
      return todos.filter(t => t.isComplete);
    default:
      return todos;
  }
};

export default (state = initState, action) => {
  console.log(state);
  console.log(action);
  switch (action.type) {
    case actionCreators.todoAdd:
      return {
        ...state,
        currentTodo: "",
        todos: state.todos.concat(action.payload)
      };
    case actionCreators.currentUpdate:
      return { ...state, currentTodo: action.payload };
    case actionCreators.todoLoad:
      return { ...state, todos: action.payload };
    case actionCreators.todoReplace:
      return {
        ...state,
        todos: state.todos.map(t => {
          return t.id === action.payload.id ? action.payload : t;
        })
      };
    case actionCreators.todoDelete:
      return {
        ...state,
        todos: state.todos.filter(t => t.id !== action.payload)
      };
    default:
      return state;
  }
};
