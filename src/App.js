import React, { Component } from "react";
import "./App.css";
import TodoForm from "./components/todoForm";
import TodoList from "./components/todoList";
import Message from "./components/message";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Footer from "./components/footer";

class App extends Component {
  render() {
    return (
      <Router>
        <div className="Todo-App">
          <Message message="Test" />
          <TodoForm />
          <Route path='/:filter?' render={({match}) => (
             <TodoList filter={match.params.filter} />
          )} />
         
          <Footer />
        </div>
      </Router>
    );
  }
}

export default App;
