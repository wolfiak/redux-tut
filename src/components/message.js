import React from 'react';
import {connect} from 'react-redux';

const message = ({ message }) => (message ? <span>{message}</span> : null);

export default connect(state=> ({message: state.message}))(message);