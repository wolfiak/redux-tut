import React, {Component} from 'react';
import { connect } from 'react-redux';
import { updateCurrent, postTodo } from '../reducers/todo';

class TodoForm extends Component {
  handleInputChange = (event) => {
    const val = event.target.value;
    this.props.updateCurrent(val);
  }

  hadnleSubmit = event => {
    event.preventDefault();
    this.props.postTodo(this.props.currentTodo);
  }

  render() {
    const {currentTodo} = this.props;
    return (
      <form onSubmit={this.hadnleSubmit}>
        <input onChange={this.handleInputChange} type="text" value={currentTodo} />
      </form>
    );
  }
}


export default connect(
  state => ({ currentTodo: state.todo.currentTodo }),
  { updateCurrent, postTodo }
)(TodoForm);
